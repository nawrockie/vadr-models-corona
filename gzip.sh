#!/bin/bash
for a in corona; do
    gzip $a.cm
    gzip $a.cm.i1f
    gzip $a.cm.i1i
    gzip $a.cm.i1m
    gzip $a.cm.i1p
    gzip $a.hmm
    gzip $a.hmm.h3f
    gzip $a.hmm.h3i
    gzip $a.hmm.h3m
    gzip $a.hmm.h3p
done
